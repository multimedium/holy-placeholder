var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var copy = require('gulp-copy');
var rev = require('gulp-rev');
var include = require("gulp-include");
var notify = require('gulp-notify');
var config = require("./gulp-config.json");
var newer = require('gulp-newer');
var autoprefixer = require('gulp-autoprefixer');

/**
 * Work
 */
gulp.task('default', function() {
    gulp.start('sass');
});

/**
 * Watch
 */
gulp.task('watch', function() {
    // Watch the files declared in the config file
    gulp.watch(config.watch.sass, ['sass']);
});

/**
 * Build css files
 */
gulp.task('sass', function() {
    gulp.src(config.sass)
        .pipe(sass())
        .on('error', onError)
        .pipe(gulp.dest('css/'))
        .pipe(n('sass completed!'));
});

// Send a notification
function n(message) {
    return notify(
        {
            'title': 'Spine',
            'message': message,
            'icon': 'https://www.multimedium.be/apple-touch-icon.png',
            // only for the last file in the pipe
            'onLast': true
        }
    );
}

/**
 * Error handler
 *
 * @param error
 */
function onError(error) {
    console.log(error.toString());
    this.emit('end');
}